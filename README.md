# Ms-Books-Store
This is a demo project in order to ilustrate the use of the technical stack used on microservices.
Contiene en su mayoria todas las herramientas y tecnologías utilizadas en el proyecto.
![Security](http://ec2-54-94-247-179.sa-east-1.compute.amazonaws.com/api/project_badges/measure?project=com.pe.cema%3Abooks-store&metric=security_rating) ![Coverage](http://ec2-54-94-247-179.sa-east-1.compute.amazonaws.com/api/project_badges/measure?project=com.pe.cema%3Abooks-store&metric=coverage) ![Quality Gate](http://ec2-54-94-247-179.sa-east-1.compute.amazonaws.com/api/project_badges/measure?project=com.pe.cema%3Abooks-store&metric=alert_status)
### Technical Stack
This is our technical stack:
* [Gradle](https\://services.gradle.org/distributions/gradle-4.10.2-bin.zip) - Wrapper 5.6.2.
* [SpringBoot](https://spring.io/blog/2019/01/11/spring-boot-1-5-19) - Versión 2.1.9.RELEASE
* [Spring JdbcTemplate]() -
* [HiariCP](https://www.baeldung.com/hikaricp) -
* [Lombok]() - versión 1.18.10 Also you have to install a plugin for IntelliJ or Eclipse.
* [Swagger]() - versión 2.7.0
* [Actuator]() - versión 1.5.19
* [JUnit]() - versión 4.12
### Run the project
For Mac or Linux environments.
```sh
$ ./gradlew clean bootRun
```
For Windows environments.
```sh
$ gradlew.bat clean bootRun
```
### Test with Swagger UI
> In order to test the services you have to go in the next link
> http://localhost:8081/books-store/swagger-ui.html

### Unit Tests and Static Code Analisys
For Mac or Linux enironments.
```sh
$ ./gradlew clean sonarqube
```
For Windows environments.
```cmd
$ gradlew.bat clean sonarqube
```
