package com.pe.cema.books;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.jul.LevelChangePropagator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import com.pe.cema.books.model.entity.Book;
import com.pe.cema.books.model.response.GenericResult;
import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import static io.restassured.RestAssured.given;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        classes = {BooksStoreApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookControllerTest {

    public static final String APPLICATION_JSON = "application/json";
    @LocalServerPort
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
        configureLogger();
    }

    @Test
    public void whenRequestAllBooksThenReturnEmptyList() {
        final Book book = createBook("Book one", "123456897643443", "Reference of the book one", "1.32");
        postNewBook(book);
        Assert.assertTrue("List with results", !given().get("/books-store/books/").getBody().as(List.class).isEmpty());
    }

    @Test
    public void whenRegisterBookThenReturnId() {
        final Book book = createBook("Book two", "36582545875836", "Reference of the book two", "4.12");
        Assert.assertTrue("Book registered", Objects.nonNull(postNewBook(book).getId()));
    }

    @Test
    public void whenSearchBookByIsbnThenReturnBookTwo() {
        final String isbn = "32145698758965";
        final Book book = createBook("Book three", isbn, "Reference of the book three", "3.21");
        postNewBook(book);
        Assert.assertEquals("Book finded", book.getIsbn(), given().with().pathParam("isbn", isbn)
                .get("/books-store/books/isbn/{isbn}").as(Book.class).getIsbn());
    }

    @Test
    public void whenUpdateBookThenReturnNoContentCode() {
        final Book book = createBook("Book four", "4785458758458", "Reference of the book four", "4.12");
        final Book persistedBook = postNewBook(book);
        Assert.assertTrue("Book updated", given().with().body(persistedBook)
                .contentType(APPLICATION_JSON).put("/books-store/books/").statusCode() == 204);
    }

    @Test
    public void whenDeleteBookThenReturnStatusCode204() {
        final Book book = postNewBook(createBook("book five", "123341241314", "reference five", "1.2"));
        Assert.assertTrue("Book deleted, status code is 204", given().with().pathParam("id",
                book.getId().toString()).delete("/books-store/books/id/{id}").getStatusCode() == 204);
    }

    @Test
    public void whenFindByIdThenReturnNotSupportedByGenericResult() {
        Assert.assertEquals("Generic Result Code -1", "-1", given().pathParam("id", 1)
                .get("/books-store/books/id/{id}").as(GenericResult.class).getCodigoRespuesta());
    }

    private void configureLogger() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        PatternLayoutEncoder ple = new PatternLayoutEncoder();
        ple.setPattern("[%X{MDCToken}] %date %level [%thread] %logger{10} [%file:%line] %msg%n");
        ple.setContext(loggerContext);
        ple.start();
        ConsoleAppender<ILoggingEvent> consoleAppender = new ConsoleAppender<>();
        consoleAppender.setContext(loggerContext);
        consoleAppender.setImmediateFlush(true);
        consoleAppender.setName("ms-book-store");
        consoleAppender.setEncoder(ple);
        consoleAppender.start();
        loggerContext.getLogger("ROOT").setLevel(Level.DEBUG);
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.addAppender(consoleAppender);
        LevelChangePropagator loggerContextListener = new LevelChangePropagator();
        loggerContextListener.setContext(loggerContext);
        loggerContext.addListener(loggerContextListener);
    }

    private Book postNewBook(Book book) {
        return given().with().body(book)
                .contentType(APPLICATION_JSON).post("/books-store/books/").as(Book.class);
    }

    private Book createBook(String name, String isbn, String reference, String ranking) {
        return Book.builder().name(name).isbn(isbn)
                .reference(reference).ranking(new BigDecimal(ranking)).build();
    }
}
