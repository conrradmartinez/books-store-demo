create table if not exists book (
   id int primary key AUTO_INCREMENT not null,
   name varchar(150),
   reference varchar(250),
   isbn varchar(100),
   ranking decimal
);