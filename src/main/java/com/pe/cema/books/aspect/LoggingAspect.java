package com.pe.cema.books.aspect;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Aspect
@Configuration
public class LoggingAspect {

    @Around("execution(public * com.pe.cema.books.service.*.*(..)) " +
        "|| execution(public * com.pe.cema.books.rest.client.*.*(..)) " +
        "|| execution(public * com.pe.cema.books.rest.controller.*.*(..))" +
        "|| execution(public * com.pe.cema.books.model.dao.*.*(..))")
    public Object loggingMetodos(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("Metodo {}, parametros de entrada: {}", joinPoint.getSignature().toShortString(),
            joinPoint.getArgs());
        Object result = joinPoint.proceed();
        log.info("Metodo {}, parametros de salida: {}", joinPoint.getSignature().toShortString(), result);
        return result;
    }
}
