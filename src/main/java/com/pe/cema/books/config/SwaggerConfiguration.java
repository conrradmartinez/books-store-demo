package com.pe.cema.books.config;

import com.pe.cema.books.config.properties.ApiSwaggerProperties;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Clase que configura Swagger.
 *
 * @author conrrad.martinez@avantica.net.
 */
@Configuration
@EnableSwagger2
@NoArgsConstructor
public class SwaggerConfiguration {

    /**
     * Bean que se utiliza para exponer la documentacion swagger.
     *
     * @return Docket con la info del swagger.
     */
    @Bean
    public Docket api(ApiSwaggerProperties apiSwaggerProperties) {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title(apiSwaggerProperties.getTitle())
                .version(apiSwaggerProperties.getVersion())
                .description(apiSwaggerProperties.getDescription())
                .build();
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo).select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any()).build()
                .useDefaultResponseMessages(false);
    }

}
