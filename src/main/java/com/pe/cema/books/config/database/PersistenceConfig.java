package com.pe.cema.books.config.database;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;

@Log4j2
@Configuration
public class PersistenceConfig {

    @Bean
    @Primary
    public DataSource dataSource(DataSourceProperties properties) {
        final HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setMaximumPoolSize(properties.getMaximunPoolSize());
        hikariDataSource.setDataSourceClassName(properties.getDataSourceClassName());
        hikariDataSource.addDataSourceProperty("url", properties.getUrl());
        hikariDataSource.addDataSourceProperty("user", properties.getUser());
        hikariDataSource.addDataSourceProperty("password", properties.getPassword());
        hikariDataSource.setInitializationFailTimeout(properties.getInitializationFailTimeOut());
        hikariDataSource.setPoolName(properties.getName());

        getSampleData(hikariDataSource);
        return hikariDataSource;
    }

    @Bean
    public SimpleJdbcInsert simpleJdbcInsertBook(DataSource dataSource) {
        return new SimpleJdbcInsert(dataSource).withTableName("book").usingGeneratedKeyColumns("id");
    }

    private void getSampleData(HikariDataSource hikariDataSource) {
        try (final Connection connection = hikariDataSource.getConnection()) {
            ScriptUtils.executeSqlScript(connection, new EncodedResource(new ClassPathResource("sql/books-create.sql"), StandardCharsets.UTF_8));
        } catch (SQLException e) {
            log.error("Excepcion ejecutando scripts", e);
        }
    }
}
