package com.pe.cema.books.config.database;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "spring.datasource.properties")
public class DataSourceProperties {

    private String dataSourceClassName;

    private String url;

    private String user;

    private String password;

    private int maximunPoolSize;

    private long initializationFailTimeOut;

    private String name;
}
