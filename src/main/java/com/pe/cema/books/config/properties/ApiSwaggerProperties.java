package com.pe.cema.books.config.properties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "api.swagger.info")
@Getter
@Setter
@NoArgsConstructor
public class ApiSwaggerProperties {

    private String title;
    private String description;
    private String version;
}
