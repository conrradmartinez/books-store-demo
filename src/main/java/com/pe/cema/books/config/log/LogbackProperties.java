package com.pe.cema.books.config.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Propiedades de configuracion para log4j2.
 *
 * @author conrrad.martinez@avantica.net.
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "api.aspect.logging")
@Configuration
@NoArgsConstructor
public class LogbackProperties {

    private String mdcTokenHeader;

    private String mdcTokenKey;

}
