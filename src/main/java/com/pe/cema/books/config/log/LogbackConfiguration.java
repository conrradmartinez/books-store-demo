package com.pe.cema.books.config.log;

import com.pe.cema.books.rest.filter.LogbackMdcFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogbackConfiguration {

    private static final int ORDER = 2;

    @Bean
    public LogbackMdcFilter logbackMdcFilter(LogbackProperties logbackProperties) {

        return new LogbackMdcFilter(logbackProperties);
    }

    @Bean
    public FilterRegistrationBean servletRegistrationBean(LogbackMdcFilter log4jMdcFilter) {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(log4jMdcFilter);
        registrationBean.setOrder(ORDER);
        return registrationBean;
    }

}
