package com.pe.cema.books.model.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Builder
@ToString
public class Book {

    private Long id;
    private String name;
    private BigDecimal ranking;
    private String isbn;
    private String reference;
}
