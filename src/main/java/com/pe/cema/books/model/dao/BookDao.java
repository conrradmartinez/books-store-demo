package com.pe.cema.books.model.dao;

import com.pe.cema.books.model.entity.Book;
import com.pe.cema.books.model.mapper.BookRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.Collections;
import java.util.List;

@Repository
public class BookDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private SimpleJdbcInsert simpleJdbcInsert;
    @Autowired
    private BookRowMapper bookRowMapper;

    public List<Book> findAll() {
        return jdbcTemplate.query("SELECT * FROM book", bookRowMapper);
    }

    public Long insertBook(Book book) {
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("name", book.getName(), Types.VARCHAR);
        in.addValue("reference", book.getReference(), Types.VARCHAR);
        in.addValue("isbn", book.getIsbn(), Types.VARCHAR);
        in.addValue("ranking", book.getRanking(), Types.DECIMAL);
        return simpleJdbcInsert.executeAndReturnKey(in).longValue();
    }

    public void updateBook(Book book) {
        jdbcTemplate.update("UPDATE book SET isbn = ?, name = ?, reference = ?, ranking = ? WHERE id = ?",
                new Object[]{book.getIsbn(), book.getName(), book.getReference(), book.getRanking(), book.getId()},
                new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.DECIMAL, Types.NUMERIC});
    }

    public Book findByIsbn(String isbn) {
        return jdbcTemplate.queryForObject("SELECT * FROM book WHERE isbn = ?", new Object[]{isbn}, bookRowMapper);
    }

    public Book findOne(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM book WHERE id = ?", new Object[]{id}, bookRowMapper);
    }

    public void deleteBook(Long id) {
        jdbcTemplate.update("DELETE FROM book WHERE id = ? ", Collections.singletonList(id).toArray());
    }
}
