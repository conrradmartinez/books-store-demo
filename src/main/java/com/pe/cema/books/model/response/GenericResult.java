package com.pe.cema.books.model.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class GenericResult {

    private String codigoRespuesta;
    private String mensajeRespuesta;
}
