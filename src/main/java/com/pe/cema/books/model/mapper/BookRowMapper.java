package com.pe.cema.books.model.mapper;

import com.pe.cema.books.model.entity.Book;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookRowMapper implements RowMapper<Book> {

    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
        final Book.BookBuilder bookBuilder = Book.builder();
        bookBuilder.id(rs.getLong("id")).name(rs.getString("name")).isbn(rs.getString("isbn"))
                .reference(rs.getString("reference")).ranking(rs.getBigDecimal("ranking"));
        return bookBuilder.build();
    }
}
