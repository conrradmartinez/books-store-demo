package com.pe.cema.books.service;

import com.pe.cema.books.model.dao.BookDao;
import com.pe.cema.books.model.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    @Autowired
    private BookDao bookDao;

    public List<Book> getAll() {
        return bookDao.findAll();
    }

    public Book getByIsbn(String isbn) {
        return bookDao.findByIsbn(isbn);
    }

    public Book registerBook(Book book) {
        return Book.builder().id(bookDao.insertBook(book)).ranking(book.getRanking())
                .isbn(book.getIsbn()).name(book.getName()).reference(book.getReference()).build();
    }

    public void updateBook(Book book) {
        bookDao.updateBook(book);
    }

    public void deleteBook(Long id) {
        Optional.ofNullable(bookDao.findOne(id))
                .ifPresent(book -> bookDao.deleteBook(book.getId()));
    }
}
