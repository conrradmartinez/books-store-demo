package com.pe.cema.books.rest.filter;

import com.pe.cema.books.config.log.LogbackProperties;
import io.micrometer.core.instrument.util.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.UUID;

public class LogbackMdcFilter extends OncePerRequestFilter {

    private LogbackProperties logbackProperties;

    public LogbackMdcFilter(LogbackProperties logbackProperties) {

        this.logbackProperties = logbackProperties;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                    final FilterChain chain)
            throws java.io.IOException, ServletException {
        try {
            final String token;
            if (!StringUtils.isEmpty(request.getHeader(logbackProperties.getMdcTokenHeader()))) {
                token = request.getHeader(logbackProperties.getMdcTokenHeader());
            } else {
                token = UUID.randomUUID().toString().toUpperCase(Locale.getDefault()).replace("-", "");
            }

            MDC.put(logbackProperties.getMdcTokenKey(), token);
            ThreadContext.put(logbackProperties.getMdcTokenKey(), token);
            response.addHeader(logbackProperties.getMdcTokenHeader(), token);
            chain.doFilter(request, response);
        } finally {
            MDC.remove(logbackProperties.getMdcTokenKey());
            ThreadContext.clearAll();
        }
    }
}
