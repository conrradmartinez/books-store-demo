package com.pe.cema.books.rest.controller;

import com.pe.cema.books.model.response.GenericResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class AdviceController {

    public static final String ERROR_INESPERADO = "Ocurrió un error inesperado";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GenericResult getErrorInterno(Exception ex) {
        logger.error(ERROR_INESPERADO, ex);
        final GenericResult genericResponse = new GenericResult();
        genericResponse.setCodigoRespuesta("-1");
        genericResponse.setMensajeRespuesta(ERROR_INESPERADO);
        return genericResponse;

    }

}
