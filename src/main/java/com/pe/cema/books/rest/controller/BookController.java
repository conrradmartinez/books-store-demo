package com.pe.cema.books.rest.controller;

import com.pe.cema.books.model.entity.Book;
import com.pe.cema.books.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/")
    public List<Book> bookList() {
        return bookService.getAll();
    }

    @GetMapping("/isbn/{isbn}")
    public Book bookByIsbn(@PathVariable("isbn") String isbn) {
        return bookService.getByIsbn(isbn);
    }

    @DeleteMapping("/id/{id}")
    public ResponseEntity removeBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public Book newBook(@RequestBody Book book) {
        return bookService.registerBook(book);
    }

    @PutMapping
    public ResponseEntity updateBook(@RequestBody Book book) {
        bookService.updateBook(book);
        return ResponseEntity.noContent().build();
    }
}
